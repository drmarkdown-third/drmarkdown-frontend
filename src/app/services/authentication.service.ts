import { environment } from './../../environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { UserModel } from './../models/user-model';
import { Injectable } from '@angular/core';
import { Observable, empty, of, throwError, pipe, BehaviorSubject } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {


  static USER_INFO = 'USER_INFO';

  private userInfoSubect: BehaviorSubject<UserModel>;
  public currentUser$: Observable<UserModel>;

  constructor(private httpClient: HttpClient,
    private cookieService: CookieService) {

    const jsonString = this.cookieService.get(AuthenticationService.USER_INFO);
    if (jsonString === '') {
      this.userInfoSubect = new BehaviorSubject<UserModel>(null);
    } else {
      this.userInfoSubect = new BehaviorSubject<UserModel>(JSON.parse(this.cookieService.get(AuthenticationService.USER_INFO)));
    }

    this.currentUser$ = this.userInfoSubect.asObservable();
  }


  login(username: string, password: string): Observable<UserModel> {

    const url = `${environment.ENDPOINTS.USER_LOGIN}`;

    return this.httpClient.post<UserModel>(url, { username, password })
      .pipe(
        map(userModel => {

          // create a cookie with jwtToken
          this.cookieService.set(AuthenticationService.USER_INFO, JSON.stringify(userModel));

          // notify everybody else
          this.userInfoSubect.next(userModel);

          return userModel;
        })
      );
  }

  logout() {
    this.cookieService.delete(AuthenticationService.USER_INFO);

    this.userInfoSubect.next(null);
  }

  registerUser(formValue: any): Observable<UserModel> {

    const url = `${environment.ENDPOINTS.USER_CREATION}`;

    return this.httpClient.post<UserModel>(
      url,
      formValue
    ).pipe(
      map(userModel => {
        // create a cookie with jwtToken
        this.cookieService.set(AuthenticationService.USER_INFO, JSON.stringify(userModel));

        // notify everybody else
        this.userInfoSubect.next(userModel);

        return userModel;
      })
    );

  }

  fetchUserInfo(userId: string): Observable<UserModel> {
    const url = `${environment.ENDPOINTS.USER_INFO}/${userId}`;
    return this.httpClient.get<UserModel>(url);
  }

  
  public get currentUserValue() {
    return this.userInfoSubect.value;
  }
}
