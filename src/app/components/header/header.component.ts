import { Router } from '@angular/router';
import { UserModel } from './../../models/user-model';
import { AuthenticationService } from '../../services/authentication.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  loggedIn = false;

  constructor(private autheticationService: AuthenticationService,
            private router: Router) {

      this.autheticationService.currentUser$.subscribe(
        userModel => this.loggedIn = userModel != null
      );
   }

   ngOnInit() {
   }


   logoutClick(event: any) {
     event.preventDefault();

     this.autheticationService.logout();
     this.router.navigate(['/home']);
   }
}
